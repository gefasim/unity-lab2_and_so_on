﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
	public static readonly string PLAY_SCENE = "SampleScene";

	public void OnCreatePlayScene()
	{
		UnityEngine.SceneManagement.SceneManager.LoadScene(PLAY_SCENE);
	}

	public void OnExit()
	{
		Application.Quit();
	}
}
